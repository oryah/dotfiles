local status, colors = pcall(require, "lsp-colors")
if not status then
	return
end

colors.setup({
	error = "#db4b4b",
	Warn = "#e0af68",
	Info = "#0db9d7",
	Hint = "#10b981",
})
