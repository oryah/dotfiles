local status, telescope = pcall(require, "telescope")
if not status then
	return
end
telescope.load_extension("live_grep_args")
local actions = require("telescope.actions")
local builtin = require("telescope.builtin")
local lga_actions = require("telescope-live-grep-args.actions")

telescope.setup({
	defaults = {
		mappings = {
			n = {
				["q"] = actions.close,
			},
		},
	},
	extensions = {
		live_grep_args = {
			auto_quoting = true, -- enable/disable auto-quoting
			mappings = {
				i = {
					["<C-k>"] = lga_actions.quote_prompt(),
					["<C-l>g"] = lga_actions.quote_prompt({ postfix = " --iglob " }),
					["<C-l>t"] = lga_actions.quote_prompt({ postfix = " -t" }),
				},
			},
		},
	},
})

local map = vim.keymap.set

map("n", ";f", function()
	builtin.find_files({ no_ignore = false, hidden = true })
end)
map("n", ";r", function()
	telescope.extensions.live_grep_args.live_grep_args()
end)
map("n", "\\\\", function()
	builtin.buffers()
end)
map("n", ";t", function()
	builtin.help_tags()
end)
map("n", ";;", function()
	builtin.resume()
end)
map("n", ";e", function()
	builtin.diagnostics()
end)
