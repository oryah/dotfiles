local map = vim.keymap.set

-- Increment/decrement
map("n", "+", "<C-a>")
map("n", "-", "<C-x>")

-- Delete a word backwards
map("n", "dw", 'vb"_d')
map("n", "вц", 'vb"_d')

-- Select all
map("n", "<C-a>", "gg<S-v>G")
map("n", "<C-ф>", "gg<S-v>G")

-- Tabs
map("n", "te", ":tabedit")
map("n", "<S-Tab>", ":tabprev<Return>")
map("n", "<Tab>", ":tabnext<Return>")
map("n", "еу", ":tabedit")

-- Split window
map("n", "ss", ":split<Return><C-w>w")
map("n", "sv", ":vsplit<Return><C-w>w")
map("n", "ыы", ":split<Return><C-w>w")
map("n", "ым", ":vsplit<Return><C-w>w")

-- Move window
map("n", "<Space>", "<C-w>w")
map("", "s<left>", "<C-w>h")
map("", "s<up>", "<C-w>k")
map("", "s<down>", "<C-w>j")
map("", "s<right>", "<C-w>l")
map("", "sh", "<C-w>h")
map("", "sk", "<C-w>k")
map("", "sj", "<C-w>j")
map("", "sl", "<C-w>l")
map("n", "<Space>", "<C-w>w")
map("", "ы<left>", "<C-w>h")
map("", "ы<up>", "<C-w>k")
map("", "ы<down>", "<C-w>j")
map("", "ы<right>", "<C-w>l")
map("", "ыр", "<C-w>h")
map("", "ыл", "<C-w>k")
map("", "ыо", "<C-w>j")
map("", "ыд", "<C-w>l")

-- Resize window
map("n", "<C-w><left>", "<C-w><")
map("n", "<C-w><right>", "<C-w>>")
map("n", "<C-w><up>", "<C-w>+")
map("n", "<C-w><down>", "<C-w>-")

-- Escape to normal mode
map("i", "jj", "<Esc>")
map("i", "оо", "<Esc>")
map("t", "<C-t>", "<C-\\><C-n>")
map("t", "<C-е>", "<C-\\><C-n>")
