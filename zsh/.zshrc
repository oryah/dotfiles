#!/usr/bin/env zsh

setopt COMPLETE_ALIASES          # Prevent aliases on the command line from being internally substituted.
setopt COMPLETE_IN_WORD          # The cursor is set to the end of the word if completion is started. 
setopt CORRECT                   # Try to correct the spelling of commands. 
setopt IGNORE_EOF                # Do not exit on end-of-file. Require the use of exit of logout instead. 
setopt NO_BG_NICE                # Do not run all background jobs with low prority.
setopt NO_HUP                    # Do not send the HUP signal to running jobs when the shell exits.
setopt NO_LIST_BEEP              # Do not beep on an ambiguous completion.

# History
setopt EXTENDED_HISTORY          # Write the history file in the ':start:elapsed;command' format.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire a duplicate event first when trimming history.
setopt HIST_IGNORE_DUPS          # Do not record an event that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete an old recorded event if a new event is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a previously found event.
setopt HIST_IGNORE_SPACE         # Do not record an event starting with a space.
setopt HIST_SAVE_NO_DUPS         # Do not write a duplicate event to the history file.
setopt HIST_VERIFY               # Do not execute immediately upon history expansion.

# PROMPT
fpath=($XDG_CONFIG_HOME/zsh/prompt $fpath)
source $XDG_CONFIG_HOME/zsh/prompt/prompt_purification_setup

# Completion
fpath=($XDG_CONFIG_HOME/zsh/completion $fpath)

# Git log find by commit message
function glf() { git log --all --grep="$1"; }

# Aliases
source $XDG_CONFIG_HOME/aliases/aliases

# Turn off vi mode
bindkey -e
export KEYTIMEOUT=1

# Enable completion
autoload -Uz compinit
compinit

