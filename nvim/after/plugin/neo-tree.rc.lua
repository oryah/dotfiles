local status, neoTree = pcall(require, "neo-tree")
if not status then
	return
end

neoTree.setup({
	close_if_last_window = true,
	window = {
		position = "right",
		float = false,
		mappings = {
			["s"] = "noop",
			["<space>"] = "noop",
			["l"] = "open",
			["h"] = "close_node",
			["<C-s>"] = "split_with_window_picker",
			["<C-v>"] = "vsplit_with_window_picker",
			["a"] = {
				"add",
				config = {
					show_path = "relative", -- "none", "relative", "absolute"
				},
			},
			["A"] = {
				"add_directory",
				config = {
					show_path = "relative", -- "none", "relative", "absolute"
				},
			},
			["c"] = {
				"copy",
				config = {
					show_path = "relative", -- "none", "relative", "absolute"
				},
			},
			["m"] = {
				"move",
				config = {
					show_path = "relative", -- "none", "relative", "absolute"
				},
			},
		},
	},
})

local opts = { silent = true, noremap = true }
local map = vim.keymap.set
map("n", "<C-n>", "<Cmd>Neotree toggle<CR>", opts)
map("n", "<C-f>", "<Cmd>Neotree action=focus<CR>", opts)
