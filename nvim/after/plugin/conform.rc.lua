local status, conform = pcall(require, "conform")
if not status then
	return
end

local utils = require("conform.util")

conform.setup({
	formatters = {
		-- currently not work, eslint:timeout https://github.com/stevearc/conform.nvim/issues/364
		eslint_d = {
			command = utils.from_node_modules("eslint_d"),
			args = { "--fix-to-stdout", "--stdin", "--stdin-filename", "$FILENAME" },
			cwd = utils.root_file({
				"package.json",
			}),
		},
		eslint = {
			command = utils.from_node_modules("eslint"),
			args = { "--fix-to-stdout", "--stdin", "--stdin-filename", "$FILENAME" },
			cwd = utils.root_file({
				"package.json",
			}),
		},
	},
	formatters_by_ft = {
		lua = { "stylua" },
		javascript = {
			{ "prettierd", "prettier" },
			{ "eslint_d" },
		},
		typescript = {
			{ "prettierd", "prettier" },
			{ "eslint_d" },
		},
		typescriptreact = {
			{ "prettierd", "prettier" },
			{ "eslint_d" },
		},
		python = { "black" },
		css = { { "prettierd", "prettier" } },
		scss = { { "prettierd", "prettier" } },
		less = { { "prettierd", "prettier" } },
		html = { { "prettierd", "prettier" } },
		json = { { "prettierd", "prettier" } },
		yaml = { { "prettierd", "prettier" } },
		markdown = { { "prettierd", "prettier" } },
		graphql = { { "prettierd", "prettier" } },
		vue = { { "prettierd", "prettier" } },
	},
})

local opts = { noremap = true, silent = true }
local map = vim.keymap.set
local format = function()
	conform.format({
		lsp_fallback = true,
		async = false,
		timeout_ms = 500,
	})
end

map("n", "fo", format, opts)
map("n", "fl", format, opts)
