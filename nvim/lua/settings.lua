if vim.loader then
	vim.loader.enable()
end

local o = vim.opt
local autocmd = vim.api.nvim_create_autocmd
local g = vim.g
local cmd = vim.cmd

-- Global
cmd("autocmd!")
o.compatible = false
o.relativenumber = true
vim.scriptencoding = "utf-8"
o.fileencodings = { "utf-8", "latin" }
o.encoding = "utf-8"
vim.noerrorbells = true
o.title = true
o.swapfile = false
o.backup = false
o.hlsearch = true
o.showcmd = true
o.cmdheight = 1
o.laststatus = 2
o.scrolloff = 10
o.expandtab = true
o.shell = "/bin/zsh"
o.shellcmdflag = "-ic" -- interactive and command flag for zsh
o.backupskip = { "/tmp/*", "/private/tmp/*" }
o.inccommand = "split"
o.signcolumn = "yes"

-- Suppress appending <PasteStart> and <PasteEnd> when pasting
-- o.t_BE = ''
o.sc = false
o.ru = false
o.sm = false

--  Don't redraw while executing macros (good performance config)
o.lazyredraw = true
--
-- Ignore case when searching
o.ignorecase = true

-- Indents
cmd("filetype plugin indent on")
o.shiftwidth = 2
o.tabstop = 2
o.ai = true -- Auto indent
o.si = true -- Smart indent
o.smarttab = true
o.wrap = false
o.backspace = { "start", "eol", "indent" }

-- Finding files - Search down into subfolders
o.path:append({ "**" })
o.wildignore:append({ "*/node_modules/**" })

-- Font
o.guifont:append({ "*" })

-- Add asterisk in block comments
o.formatoptions:append({ "r" })

-- Folding
o.foldlevel = 20
o.foldmethod = "expr"
o.foldexpr = "nvim_treesitter#foldexpr()"

-- Turn off paste mode when leaving insert
autocmd("InsertLeave", {
	pattern = "*",
	command = "set nopaste",
})

-- Highlights
cmd("syntax on")
o.cursorline = true
o.termguicolors = true
o.winblend = 0
o.wildoptions = "pum"
o.pumblend = 5

-- Russian keymaps
o.langmap =
	"ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz"

-- Disable nerrw at start
g.loaded_netrw = 1
g.loaded_netrwPlugin = 1
