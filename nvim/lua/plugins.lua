local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end

vim.opt.rtp:prepend(lazypath)

local plugins = {
	-- Theme (nightfox.rc.lua)
	{
		"EdenEast/nightfox.nvim",
		priority = 1000,
		setup = true,
	},
	-- Language Server Protocol (lspconfig.rc.lua)
	"neovim/nvim-lspconfig",
	"williamboman/mason-lspconfig.nvim",
	-- mason (mason.rc.lua)
	{
		"williamboman/mason.nvim",
		build = ":MasonUpdate",
	},
	-- Lightweight yet powerful formatter (conform.rc.lua)
	{
		"stevearc/conform.nvim",
		lazy = false,
	},
	-- An asynchronous linter plugin (lint.rc.lua)
	{
		"mfussenegger/nvim-lint",
		lazy = false,
		event = { "BufReadPre", "BufNewFile" },
	},
	-- Lsp hit colors (lsp-colors.rc.vim)
	"folke/lsp-colors.nvim",
	-- lspsaga.rc.vim
	{
		"glepnir/lspsaga.nvim",
		dependencies = {
			"nvim-tree/nvim-web-devicons",
			"nvim-treesitter/nvim-treesitter",
		},
	},
	-- Completion (cmp.rc.vim, lspkind.rc.lua),
	{
		"hrsh7th/nvim-cmp",
		dependencies = {
			"hrsh7th/cmp-path",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-nvim-lsp",
			"onsails/lspkind-nvim",
			"saadparwaiz1/cmp_luasnip",
			{
				"L3MON4D3/LuaSnip",
				version = "v2.*",
				build = "make install_jsregexp",
				dependencies = {
					"rafamadriz/friendly-snippets",
				},
			},
		},
	},
	-- Status line (lualine.rc.lua)
	"nvim-lualine/lualine.nvim",
	-- Git (gitsign.rc.lua, git.rc.lua),
	"dinhhuy258/git.nvim",
	"lewis6991/gitsigns.nvim",
	-- Lsp manager  (lsp-installer.rc.lua)
	"williamboman/nvim-lsp-installer",
	-- treesitter.rc.lua
	{
		"nvim-treesitter/nvim-treesitter",
		lazy = true,
		build = ":TSUpdate",
		event = { "BufReadPost", "BufNewFile" },
		dependencies = {
			{
				"nvim-treesitter/nvim-treesitter-textobjects",
				init = function()
					-- PERF: no need to load the plugin, if we only need its queries for mini.ai
					local plugin = require("lazy.core.config").spec.plugins["nvim-treesitter"]
					local opts = require("lazy.core.plugin").values(plugin, "opts", false)
					local enabled = false
					if opts.textobjects then
						for _, mod in ipairs({ "move", "select", "swap", "lsp_interop" }) do
							if opts.textobjects[mod] and opts.textobjects[mod].enable then
								enabled = true
								break
							end
						end
					end
					if not enabled then
						require("lazy.core.loader").disable_rtp_plugin("nvim-treesitter-textobjects")
					end
				end,
			},
		},
	},
	{
		"nvim-tree/nvim-tree.lua",
		version = "*",
		lazy = false,
		dependencies = {
			"nvim-tree/nvim-web-devicons",
		},
	},
	-- File management (telescope.rc.vim)
	"nvim-lua/plenary.nvim",
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			"nvim-telescope/telescope-live-grep-args.nvim",
		},
	},
	-- autopairs.rc.lua
	"windwp/nvim-autopairs",
	-- ts-autotag.rc.lua
	"windwp/nvim-ts-autotag",
	-- colorizer.rc.lua
	"norcalli/nvim-colorizer.lua",
}

local opts = {}

require("lazy").setup(plugins, opts)
