local status_dap, dap = pcall(require, "dap")
if not status_dap then
	return
end
local status_dap_vscode_js, dap_vscode_js = pcall(require, "dap-vscode-js")
if not status_dap_vscode_js then
	return
end
local status_dapui, dapui = pcall(require, "dapui")
if not status_dapui then
	return
end

local node_lang = {
	"typescript",
	"javascript",
	"typescriptreact",
	"javascriptreact",
	"vue",
}
local opts = { noremap = true, silent = true }
local map = vim.keymap.set

-- fancy UI for the debugger
dapui.setup({})
dap.listeners.after.event_initialized["dapui_config"] = function()
	dapui.open({})
end
dap.listeners.before.event_terminated["dapui_config"] = function()
	dapui.close({})
end
dap.listeners.before.event_exited["dapui_config"] = function()
	dapui.close({})
end
map("n", "<leader>du", function()
	require("dapui").toggle({})
end, opts)
map("n", "<leader>de", function()
	require("dapui").eval({})
end, opts)

for _, language in ipairs(node_lang) do
	dap.configurations[language] = {
		-- Debug single nodejs files
		{
			type = "pwa-node",
			request = "launch",
			name = "Launch file",
			program = "${file}",
			cwd = vim.fn.getcwd(),
			sourceMaps = true,
		},
		-- Debug nodejs processes (make sure to add --inspect when you run the process)
		{
			type = "pwa-node",
			request = "attach",
			name = "Attach",
			processId = require("dap.utils").pick_process,
			cwd = vim.fn.getcwd(),
			sourceMaps = true,
		},
		-- Debug web applications (client side)
		{
			type = "pwa-chrome",
			request = "launch",
			name = "Launch & Debug Chrome",
			url = function()
				local co = coroutine.running()
				return coroutine.create(function()
					vim.ui.input({
						prompt = "Enter URL: ",
						default = "http://localhost:3000",
					}, function(url)
						if url == nil or url == "" then
							return
						else
							coroutine.resume(co, url)
						end
					end)
				end)
			end,
			webRoot = vim.fn.getcwd(),
			protocol = "inspector",
			sourceMaps = true,
			userDataDir = false,
		},
		-- Divider for the launch.json derived configs
		{
			name = "----- ↓ launch.json configs ↓ -----",
			type = "",
			request = "launch",
		},
	}
end

-- Dap adapters
dap_vscode_js.setup({
	-- Path of node executable. Defaults to $NODE_PATH, and then "node"
	-- node_path = "node",

	-- Path to vscode-js-debug installation.
	debugger_path = vim.fn.resolve(vim.fn.stdpath("data") .. "/lazy/vscode-js-debug"),

	-- Command to use to launch the debug server. Takes precedence over "node_path" and "debugger_path"
	-- debugger_cmd = { "js-debug-adapter" },

	-- which adapters to register in nvim-dap
	adapters = {
		"chrome",
		"pwa-node",
		"pwa-chrome",
		"pwa-msedge",
		"pwa-extensionHost",
		"node-terminal",
	},

	-- Path for file logging
	-- log_file_path = "(stdpath cache)/dap_vscode_js.log",

	-- Logging level for output to file. Set to false to disable logging.
	-- log_file_level = false,

	-- Logging level for output to console. Set to false to disable console output.
	-- log_console_level = vim.log.levels.ERROR,
})

map("n", "<leader>dB", function()
	dap.set_breakpoint(vim.fn.input("Breakpoint condition: "))
end, opts)
map("n", "<leader>db", function()
	dap.toggle_breakpoint()
end, opts)
map("n", "<leader>dc", function()
	dap.continue()
end, opts)
map("n", "<leader>da", function()
	if vim.fn.filereadable(".vscode/launch.json") then
		local dap_vscode = require("dap.ext.vscode")
		dap_vscode.load_launchjs(nil, {
			["pwa-node"] = node_lang,
			["node"] = node_lang,
			["chrome"] = node_lang,
			["pwa-chrome"] = node_lang,
		})
	end
	dap.continue()
end, opts)
map("n", "<leader>dC", function()
	dap.cursor()
end, opts)
map("n", "<leader>dg", function()
	dap.goto_()
end, opts)
map("n", "<leader>di", function()
	dap.step_into()
end, opts)
map("n", "<leader>dj", function()
	dap.down()
end, opts)
map("n", "<leader>dk", function()
	dap.up()
end, opts)
map("n", "<leader>dl", function()
	dap.run_last()
end, opts)
map("n", "<leader>do", function()
	dap.step_out()
end, opts)
map("n", "<leader>dO", function()
	dap.step_over()
end, opts)
map("n", "<leader>dp", function()
	dap.pause()
end, opts)
map("n", "<leader>dr", function()
	dap.repl.toggle()
end, opts)
map("n", "<leader>ds", function()
	dap.session()
end, opts)
map("n", "<leader>dt", function()
	dap.terminate()
end, opts)
