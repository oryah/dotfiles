local status_mason, mason = pcall(require, "mason")
if not status_mason then
	return
end
local status_lspconfig, lspconfig = pcall(require, "mason-lspconfig")
if not status_lspconfig then
	return
end

mason.setup({
	ensure_installed = {
		"black",
		"cssmodules_ls",
		"diagnosticls",
		"docker-compose-language-service",
		"dockerfile-language-server",
		"eslint",
		"eslint-lsp",
		"eslint_d",
		"fixjson",
		"intelephense",
		"lua-language-server",
		"lua_ls",
		"prettier",
		"prettierd",
		"pyright",
		"stylelint",
		"stylelint_lsp",
		"stylua",
		"tailwindcss",
		"tailwindcss-language-server",
		"tsserver",
		"typescript-language-server",
		"volar",
		"vuels",
		"yaml-language-server",
	},
})

lspconfig.setup({
	automatic_installation = true,
})
