local status, saga = pcall(require, "lspsaga")
if not status then
	return
end

saga.setup({
	ui = {
		theme = "round",
		winblend = 10,
		border = "rounded",
		colors = {
			normal_bg = "#002b36",
		},
	},
  definition = {
    width = 0.62,
    height = 0.62,
  },
	symbol_in_winbar = {
		enable = false,
	},
})

local opts = { noremap = true, silent = true }
local map = vim.keymap.set
map("n", "<C-j>", "<Cmd>Lspsaga diagnostic_jump_next<CR>", opts)
map("n", "<C-k>", "<Cmd>Lspsaga diagnostic_jump_prev<CR>", opts)
map("n", "gl", "<Cmd>Lspsaga show_line_diagnostics<CR>", opts)
map("n", "gf", "<Cmd>Lspsaga finder tyd+ref+def<CR>", opts)
map("n", "gd", "<Cmd>Lspsaga goto_definition<CR>", opts)
map("n", "gp", "<Cmd>Lspsaga peek_definition<CR>", opts)
map("n", "gr", "<Cmd>Lspsaga rename<CR>", opts)
map("n", "K", "<Cmd>Lspsaga hover_doc<CR>", opts)

-- code action
local codeaction = require("lspsaga.codeaction")

map("n", "<leader>ca", function()
	codeaction:code_action()
end, { silent = true })
map("v", "<leader>ca", function()
	vim.fn.feedkeys(vim.api.nvim_replace_termcodes("<C-U>", true, false, true))
	codeaction:range_code_action()
end, { silent = true })
