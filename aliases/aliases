#!/usr/bin/env bash

# System
alias shutdown='sudo shutdown now'
alias restart='sudo reboot'
alias suspend='sudo pm-suspend'
alias bigf= 'find / -xdev -type f -size +500M'  # display "big" files > 500M

# ZSH 
alias d='dirs -v'
for index ({1..9}) alias "$index"="cd +${index}"; unset index # directory stack

alias kitty='kitty -o allow_remote_control=yes --single-instance --listen-on unix:@mykitty'

# ls
alias ls='ls --color=auto'
alias l='ls -l'
alias ll='ls -lahF'
alias lls='ls -lahFtr'
alias la='ls -A'
alias lc='ls -CF'

# wget
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"

# cp
alias cp='cp -iv'
alias mv='mv -iv'
alias rm='rm -iv'

# grep
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Dust
alias dust='du -sh * | sort -hr'

# Golang
alias gob="go build"
alias gor="go run" 
alias goc="go clean -i"
alias gta="go test ./..."       # go test all
alias gia="go install ./..."    # go install all
alias gosrc="$GOPATH/src/"      # golang src
alias gobin="$GOPATH/bin/"      # golang bin

# NVIM
alias vim="nvim"
alias vi="nvim"
alias oldvim="\vim"
alias vimdiff="nvim -d"
alias nvimc='rm -I $VIMCONFIG/swap/*'             # clean nvim swap file
alias nvimcu='rm -I $VIMCONFIG/undo/*'            # clean the vim undo
alias nviml='nvim -w $VIMCONFIG/vimlog "$@"'      # log the keystrokes 
alias nvimd='nvim --noplugin -u NONE'             # launch nvim without any plugin or config (nvim debug)
alias nvimfr='nvim +e /tmp/scratchpad.md +"set spelllang=fr"'

# Git
alias ga='git add'
alias gaa='git add .'
alias gaaa='git add --all'
alias gau='git add --update'
alias gb='git branch'
alias gbd='git branch --delete '
alias gc='git commit'
alias gcm='git commit --message'
alias gcf='git commit --fixup'
alias gco='git checkout'
alias gcob='git checkout -b'
alias gcom='git checkout master'
alias gcos='git checkout staging'
alias gcod='git checkout develop'
alias gd='git diff'
alias gda='git diff HEAD'
alias gi='git init'
alias glg='git log --graph --oneline --decorate --all'
alias gld='git log --pretty=format:"%h %ad %s" --date=short --all'
alias gm='git merge --no-ff'
alias gma='git merge --abort'
alias gmc='git merge --continue'
alias gp='git pull'
alias gpr='git pull --rebase'
alias gr='git rebase'
alias gs='git status'
alias gss='git status --short'
alias gst='git stash'
alias gsta='git stash apply'
alias gstd='git stash drop'
alias gstl='git stash list'
alias gstp='git stash pop'
alias gsts='git stash save'

# Docker
alias dls="docker container ls | awk 'NR > 1 {print \$NF}'"                  # display names of running containers
alias dR='docker rm $(docker ps -a -q)'                                      # delete every containers
alias dRr='docker rm $(docker ps -a -q) && docker rmi $(docker images -q)'   # delete every containers / images
alias dsts='docker stats $(docker ps -q)'                                    # stats on images
alias dimg='docker images'                                                   # list images installed
alias dprune='docker system prune -a'                                        # prune everything
alias dceu='docker-compose run --rm -u $(id -u):$(id -g)'                    # run as the host user
alias dce='docker-compose run --rm'

# Folders
alias work="$HOME/workspace"
alias doc="$HOME/Documents"
alias dow="$HOME/Downloads"
alias dot="$HOME/.dotfiles"

# Python
alias pip='pip3'

